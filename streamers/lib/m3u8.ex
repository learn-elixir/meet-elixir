defmodule Streamers.M3U8 do
    defstruct program_id: nil, path: nil, bandwidth: nil, ts_files: []
end